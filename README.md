**A traffic simulation - Bacca Riccardo, Battistini Riccardo, Camporesi 
Stefano, Ionut Lazar Adrian - TrafficSimulator**

Il gruppo si pone come obiettivo quello di realizzare un'applicazione 
per analizzare il flusso del traffico in uno scenario semplificato.

Applicando modelli matematici ai sistemi di trasporto è possibile 
realizzare del softwareper sostenere la realizzazione, pianificazione e 
design dei sistemi di trasporto.

Il campo della simulazione dei trasporti è nato circa quarant'anni fa e 
ad oggi costituisce un'importante branca dell'ingegneria del traffico e 
della pianificazione dei trasporti.

Il software realizzato in tale ambito contribuisce notevolmente alla 
gestione delle reti di trasporto ed è impiegato da agenzie per il 
trasporto a livello locale e nazionale.

La sua utilità sta sia nella possibilità di studiare modelli 
estremamente complessi tramite studi sperimentali sia per l'opportunità 
di produrre dimostrazioni visualmente gradevoli di scenari presenti e 
futuri.

Come modello di traffico abbiamo scelto di impiegare l'automa cellulare,
si vuole quindi studiare l'evoluzione di un sistema complesso discreto 
che generi casualità a partire da regole deterministiche (gioco della 
vita).

**Funzionalità minime ritenute obbligatorie:**

- Modellare il comportamento degli automobilisti secondo il 
  car-following model
- Implementazione scenario "rotonda" con relativa GUI
- Programmazione multi-thread per gestire le singole macchine
- Produzione di grafici e possibilità di modificare i parametri 
  sperimentali

**Funzionalità opzionali:**

- Modellare il comportamento degli automobilisti secondo il 
  lane-changing model
- Implementazione dello scenario "immissione con rampa" con relativa GUI
- Implementazione di ostacoli al traffico per studiare la variazione dei
  flussi
- Implementazione di status globali in grado di influenzare lo scenario 
  (pendenza oppure meteo)

**"Challenge" principali:**

- La realizzazione di grafici richiederà l'uso di una libreria da 
  identificare
- Sarà necessario prevedere una facile estendibilità per quanto riguarda
  la realizzazione di nuovi scenari
- Sarà necessario prevedere un certo grado di estendibilità anche per 
  gli scenari già implementati (numero di corsie ad esempio o 
  possibilità di inserire ulteriori parametri)
- I modelli matematici che rendono realisticamente il comportamento 
  degli automobilisti sono complessi e  richiedono semplificazione
- La realizzazione dell'interfaccia grafica secondo la tecnica del 
  "mosaico" non è di banale realizzazione e richiederà l'impiego di 
  librerie da identificare

**Suddivisione del lavoro:**

Bacca;
Battistini;
Camporesi;
Ionut Lazar.